local config = require("lapis.config")

config({"development", "production"},
	{
		email_enabled = false,
		postgres = {
			host = "127.0.0.1",
			port = "5432",
			user = "webman",
			database = "wtju8"
			}
		})

config("development",
	{
		code_cache = "off",
		port = "8010"
		})

config("production",
	{
		code_cache = "on",
		port = "8011",
		logging = {
			queries = false
			}
		})
